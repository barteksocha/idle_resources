﻿using idle_res_lib.Logger;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.Resources;
using idle_res_lib.State.Serializables;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace idle_res_lib_tests.Resources
{
    [TestFixture]
    public class ResourceCollectionFactoryUnitTests
    {
        private Mock<ILogger> _logger;
        private Mock<IResourceIdentifierFactory> _resourceIdentifierFactory;
        private Mock<IResourceFactory> _resourceFactory;

        private Mock<IResource> _resource1;
        private Mock<IResource> _resource2;
        private Mock<IResource> _resource3;

        private Mock<IResourceIdentifier> _resourceIdentifier1;
        private Mock<IResourceIdentifier> _resourceIdentifier2;
        private Mock<IResourceIdentifier> _resourceIdentifier3;

        private ResourceCollectionFactory _resourceCollectionFactory;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger>();
            _resourceIdentifierFactory = new Mock<IResourceIdentifierFactory>();
            _resourceFactory = new Mock<IResourceFactory>();

            _resource1 = new Mock<IResource>();
            _resource2 = new Mock<IResource>();
            _resource3 = new Mock<IResource>();

            _resourceIdentifier1 = new Mock<IResourceIdentifier>();
            _resourceIdentifier2 = new Mock<IResourceIdentifier>();
            _resourceIdentifier3 = new Mock<IResourceIdentifier>();

            _resourceFactory.Setup(rf => rf.CreateResource(_resourceIdentifier1.Object, It.IsAny<StateRoot>())).Returns(_resource1.Object);
            _resourceFactory.Setup(rf => rf.CreateResource(_resourceIdentifier2.Object, It.IsAny<StateRoot>())).Returns(_resource2.Object);
            _resourceFactory.Setup(rf => rf.CreateResource(_resourceIdentifier3.Object, It.IsAny<StateRoot>())).Returns(_resource3.Object);

            _resourceIdentifierFactory.Setup(rif => rif.Create(It.IsAny<ReferenceDataRoot>())).Returns(new List<IResourceIdentifier>());

            _resourceCollectionFactory = new ResourceCollectionFactory(_logger.Object, _resourceIdentifierFactory.Object, _resourceFactory.Object);

            _resourceIdentifierFactory.Setup(rif => rif.Create(It.IsAny<ReferenceDataRoot>())).Returns(new List<IResourceIdentifier>()
            {
                _resourceIdentifier1.Object, _resourceIdentifier2.Object, _resourceIdentifier3.Object
            });
        }

        [Test]
        public void ResourceCollectionCreationShouldBeLogged()
        {
            // act
            _resourceCollectionFactory.CreateResources(null, null);

            // assert
            _logger.Verify(l => l.Info("Creating resource collection", It.IsAny<string>(), It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void ResourceGenerationShouldWorkProperly()
        {
            // act
            List<IResource> result = _resourceCollectionFactory.CreateResources(null, null);

            // assert
            Assert.AreEqual(_resource1.Object, result[0]);
            Assert.AreEqual(_resource2.Object, result[1]);
            Assert.AreEqual(_resource3.Object, result[2]);
        }
    }
}
