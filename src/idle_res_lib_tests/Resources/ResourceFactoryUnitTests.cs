﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using idle_res_lib.Resources;
using idle_res_lib.State.Serializables;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace idle_res_lib_tests.TestCaseSources
{
    [TestFixture]
    class ResourceFactoryUnitTests
    {
        private ResourceFactory _resourceFactory;

        private Mock<ILogger> _logger;
        private Mock<IResourceIdentifier> _resourceIdentifier;

        private StateRoot _stateData;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger>();
            _resourceIdentifier = new Mock<IResourceIdentifier>();

            _resourceFactory = new ResourceFactory(_logger.Object);

            _stateData = new StateRoot
            {
                ResourceCollection = new ResourceCollectionState
                {
                    Resources = new List<ResourceState>
                    {
                    }
                }
            };
        }

        [TestCase("Creating resource Meat", ResourceType.Meat)]
        [TestCase("Creating resource Stone", ResourceType.Stone)]
        [TestCase("Creating resource WoodenWeapons", ResourceType.WoodenWeapons)]
        public void InformationAboutCreatingObjectShouldBePutIntoLogger(string expectedLog, ResourceType resourceType)
        {
            // arrange
            _resourceIdentifier.SetupGet(ri => ri.Type).Returns(resourceType);

            // act
            _resourceFactory.CreateResource(_resourceIdentifier.Object, _stateData);

            // assert
            _logger.Verify(l => l.Info(expectedLog, It.IsAny<string>(), It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void CreatingResourceWithNullStateDataShouldResultWithNullReferenceException()
        {
            // act
            Assert.Throws<NullReferenceException>(() => _resourceFactory.CreateResource(_resourceIdentifier.Object, null));
        }

        [Test]
        public void CreatingResourceWithNullResourceCollectionInStateDataShouldResultWithNullReferenceException()
        {
            // arrange
            _stateData.ResourceCollection = null;

            // act
            Assert.Throws<NullReferenceException>(() => _resourceFactory.CreateResource(_resourceIdentifier.Object, _stateData));
        }

        [Test]
        public void CreatingResourceWithNullResourcesListInResourceCollectionInStateDataShouldResultWithArgumentNullException()
        {
            // arrange
            _stateData.ResourceCollection.Resources = null;

            // act
            Assert.Throws<ArgumentNullException>(() => _resourceFactory.CreateResource(_resourceIdentifier.Object, _stateData));
        }

        [Test]
        public void ResourceAmountShouldBeZeroIfResourcesDataInStateDataIsEmpty()
        {
            // arrange
            _resourceIdentifier.SetupGet(ri => ri.Type).Returns(ResourceType.ForestFruits);

            // act
            IResource resource = _resourceFactory.CreateResource(_resourceIdentifier.Object, _stateData);

            // assert
            Assert.AreEqual(0, resource.Amount);
        }

        [TestCaseSource(typeof(FloatTestCaseSources), nameof(FloatTestCaseSources.Random100Floats))]
        public void ResourceAmountShouldBeSetProperlyIfStateDataContainInformationAboutResourceAmount(float resourceAmount)
        {
            // arrange
            _resourceIdentifier.SetupGet(ri => ri.Type).Returns(ResourceType.ForestFruits);

            // act
            IResource resource = _resourceFactory.CreateResource(_resourceIdentifier.Object, _stateData);

            // assert
            Assert.AreEqual(0, resource.Amount);
        }
    }
}
