﻿using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.Resources;
using idle_res_lib.Tiers;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace idle_res_lib_tests.Resources
{
    [TestFixture]
    class ResourceIdentifierFactoryUnitTests
    {
        private ResourceIdentifierFactory _resourceIdentifierFactory;

        private ReferenceDataRoot _referenceData;

        [SetUp]
        public void SetUp()
        {
            _resourceIdentifierFactory = new ResourceIdentifierFactory();
            _referenceData = new ReferenceDataRoot
            {
                Resources = new XmlResourceIdentifierCollection()
                {
                    ResourceIdentifiers = new XmlResourceIdentifier[]
                    {
                        new XmlResourceIdentifier
                        {
                            Price = 2.0f,
                            TierIndex = 1,
                            Type = "Wood"
                        },
                        new XmlResourceIdentifier
                        {
                            Price = 20.0f,
                            TierIndex = 2,
                            Type = "Stone"
                        }
                    }
                }
            };
        }

        [Test]
        public void CreatingResourceIdentifiersShouldThrowNullReferenceExceptionWhenReferenceDataIsNull()
        {
            // act
            Assert.Throws<NullReferenceException>(() => _resourceIdentifierFactory.Create(null));
        }

        [Test]
        public void CreatingResourceIdentifiersShouldThrowNullReferenceExceptionWhenResourcesInReferenceDataIsNull()
        {
            // arrange
            _referenceData.Resources = null;

            // act
            Assert.Throws<NullReferenceException>(() => _resourceIdentifierFactory.Create(_referenceData));
        }

        [Test]
        public void CreatingResourceIdentifiersShouldThrowArgumentNullExceptionWhenResourcesCollectionInResourcesInReferenceDataIsNull()
        {
            // arrange
            _referenceData.Resources.ResourceIdentifiers = null;

            // act
            Assert.Throws<ArgumentNullException>(() => _resourceIdentifierFactory.Create(_referenceData));
        }

        [Test]
        public void CreatingResourceIdentifiersShouldWorkProperly()
        {
            // act
            List<IResourceIdentifier> resourceIdentifiers = _resourceIdentifierFactory.Create(_referenceData);

            // assert
            Assert.AreEqual(2, resourceIdentifiers.Count);

            Assert.AreEqual(ResourceType.Wood, resourceIdentifiers[0].Type);
            Assert.AreEqual(2.0f, resourceIdentifiers[0].BasePrice);
            Assert.AreEqual(TierType.PreStoneAge, resourceIdentifiers[0].Tier);

            Assert.AreEqual(ResourceType.Stone, resourceIdentifiers[1].Type);
            Assert.AreEqual(20.0f, resourceIdentifiers[1].BasePrice);
            Assert.AreEqual(TierType.StoneAge, resourceIdentifiers[1].Tier);
        }
    }
}
