﻿using System;
using System.Collections.Generic;

namespace idle_res_lib_tests.TestCaseSources
{
    public static class StringTestCaseSources
    {
        [Flags]
        public enum GenerationMode
        {
            None = 0,
            LowerCase = 1,
            UpperCase = 1 << 1,
            Digits = 1 << 2,
            Space = 1 << 3
        }

        public static IEnumerable<string> UpperLowerDigitsSpaceUpTo1000CharactersSourceOf100Strings => GetTestCaseSource(
            0,
            1000,
            100,
            GenerationMode.Digits | GenerationMode.LowerCase | GenerationMode.Space | GenerationMode.UpperCase);

        private static string lowercaseCharactersSource = "abcdefghijklmnopqrstuvwxyz";
        private static string uppercaseCharactersSource = lowercaseCharactersSource.ToUpper();
        private static string digitsSource = "0123456789";
        private static string spaceSource = " ";

        private static IEnumerable<string> GetTestCaseSource(int mininmumLength, int maximumLength, int count, GenerationMode mode)
        {
            if (mode != GenerationMode.None)
            {
                Random random = new Random();

                string charactersSource = string.Empty;
                if (mode.HasFlag(GenerationMode.LowerCase))
                {
                    charactersSource += lowercaseCharactersSource;
                }
                if (mode.HasFlag(GenerationMode.UpperCase))
                {
                    charactersSource += uppercaseCharactersSource;
                }
                if (mode.HasFlag(GenerationMode.Digits))
                {
                    charactersSource += digitsSource;
                }
                if (mode.HasFlag(GenerationMode.Space))
                {
                    charactersSource += spaceSource;
                }

                for (int i = 0; i < count; ++i)
                {
                    int stringLength = random.Next(mininmumLength, maximumLength);

                    string result = string.Empty;
                    for (int j = 0; j < stringLength; ++j)
                    {
                        result += charactersSource[random.Next(charactersSource.Length)];
                    }

                    yield return result;
                }
            }
        }
    }
}
