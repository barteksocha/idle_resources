﻿using System;
using System.Collections.Generic;

namespace idle_res_lib_tests.TestCaseSources
{
    public static class FloatTestCaseSources
    {
        public static IEnumerable<float> Random100Floats => GetTestCaseSource(-100.0f, 100.0f, 100);

        private static IEnumerable<float> GetTestCaseSource(float minimum, float maximum, int count)
        {
            Random random = new Random();

            for (int i = 0; i < count; ++i)
            {
                yield return minimum + ((float)random.NextDouble() * (maximum - minimum));
            }
        }
    }
}
