﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using Moq;
using NUnit.Framework;

namespace idle_res_lib_tests.Logger
{
    [TestFixture]
    public class VisualStudioConsoleLoggerUnitTests
    {
        private VisualStudioConsoleLogger _visualStudioConsoleLogger;
        private Mock<IDateTimeWrapper> _dateTimeWrapper;
        private Mock<IDebugDiagnosticsWrapper> _debugDiagnosticsWrapper;

        [SetUp]
        public void SetUp()
        {
            _dateTimeWrapper = new Mock<IDateTimeWrapper>();
            _debugDiagnosticsWrapper = new Mock<IDebugDiagnosticsWrapper>();
            _visualStudioConsoleLogger = new VisualStudioConsoleLogger(_dateTimeWrapper.Object, _debugDiagnosticsWrapper.Object);
        }

        [Test]
        public void DefaultSeverityForVisualStudioConsoleLoggerShouldBeWarning()
        {
            // assert
            Assert.AreEqual(Severity.Warning, _visualStudioConsoleLogger.CurrentSeverity);
        }

        [TestCase(false, Severity.Debug, Severity.Info)]
        [TestCase(true, Severity.Info, Severity.Info)]
        [TestCase(true, Severity.Warning, Severity.Info)]
        [TestCase(false, Severity.Warning, Severity.Fatal)]
        [TestCase(true, Severity.Fatal, Severity.Fatal)]
        public void VerifyIfLoggingInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggingSeverity, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Log("message", loggingSeverity);

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(false, Severity.Debug)]
        [TestCase(false, Severity.Info)]
        [TestCase(false, Severity.Warning)]
        [TestCase(false, Severity.Error)]
        [TestCase(false, Severity.Fatal)]
        public void VerifyIfTracingInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Trace("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(true, Severity.Debug)]
        [TestCase(false, Severity.Info)]
        [TestCase(false, Severity.Warning)]
        [TestCase(false, Severity.Error)]
        [TestCase(false, Severity.Fatal)]
        public void VerifyIfDebuggingInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Debug("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(true, Severity.Debug)]
        [TestCase(true, Severity.Info)]
        [TestCase(false, Severity.Warning)]
        [TestCase(false, Severity.Error)]
        [TestCase(false, Severity.Fatal)]
        public void VerifyIfInformingInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Info("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(true, Severity.Debug)]
        [TestCase(true, Severity.Info)]
        [TestCase(true, Severity.Warning)]
        [TestCase(false, Severity.Error)]
        [TestCase(false, Severity.Fatal)]
        public void VerifyIfWarningInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Warning("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(true, Severity.Debug)]
        [TestCase(true, Severity.Info)]
        [TestCase(true, Severity.Warning)]
        [TestCase(true, Severity.Error)]
        [TestCase(false, Severity.Fatal)]
        public void VerifyIfErroringInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Error("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        [TestCase(true, Severity.Trace)]
        [TestCase(true, Severity.Debug)]
        [TestCase(true, Severity.Info)]
        [TestCase(true, Severity.Warning)]
        [TestCase(true, Severity.Error)]
        [TestCase(true, Severity.Fatal)]
        public void VerifyIfFatalingInRegardsToSeverityWorksAsExpected(bool wasWriteLineCalled, Severity loggerSeverity)
        {
            // arrange
            _visualStudioConsoleLogger.CurrentSeverity = loggerSeverity;

            // act
            _visualStudioConsoleLogger.Fatal("message");

            // assert
            _debugDiagnosticsWrapper.Verify(dd => dd.WriteLine(It.IsAny<string>()), Times.Exactly(wasWriteLineCalled ? 1 : 0));
        }

        //TODO: verify message created
    }
}
