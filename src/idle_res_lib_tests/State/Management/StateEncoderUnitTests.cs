﻿using essentials.Wrappers;
using idle_res_lib.Resources;
using idle_res_lib.State.Management;
using idle_res_lib.State.Serializables;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace idle_res_lib_tests.State.Management
{
    [TestFixture]
    public class StateEncoderUnitTests
    {
        private StateEncoder _stateEncoder;

        private Mock<IXmlSerializerWrapper<StateRoot>> _stateSerializer;
        private Mock<IStorage> _storage;

        private Mock<IResource> _resource1;
        private Mock<IResource> _resource2;
        private Mock<IResource> _resource3;

        private Mock<IResourceIdentifier> _resourceIdentifier1;
        private Mock<IResourceIdentifier> _resourceIdentifier2;
        private Mock<IResourceIdentifier> _resourceIdentifier3;

        private StateRoot _generatedState;

        [SetUp]
        public void SetUp()
        {
            _stateSerializer = new Mock<IXmlSerializerWrapper<StateRoot>>();
            _storage = new Mock<IStorage>();

            _resource1 = new Mock<IResource>();
            _resource2 = new Mock<IResource>();
            _resource3 = new Mock<IResource>();

            _resourceIdentifier1 = new Mock<IResourceIdentifier>();
            _resourceIdentifier2 = new Mock<IResourceIdentifier>();
            _resourceIdentifier3 = new Mock<IResourceIdentifier>();

            _resource1.SetupGet(r => r.Amount).Returns(1.0f);
            _resource2.SetupGet(r => r.Amount).Returns(0.0f);
            _resource3.SetupGet(r => r.Amount).Returns(20.0f);

            _resource1.SetupGet(r => r.Identifier).Returns(_resourceIdentifier1.Object);
            _resource2.SetupGet(r => r.Identifier).Returns(_resourceIdentifier2.Object);
            _resource3.SetupGet(r => r.Identifier).Returns(_resourceIdentifier3.Object);

            _resourceIdentifier1.SetupGet(ri => ri.Type).Returns(ResourceType.Meat);
            _resourceIdentifier2.SetupGet(ri => ri.Type).Returns(ResourceType.WoodenWeapons);
            _resourceIdentifier3.SetupGet(ri => ri.Type).Returns(ResourceType.Stone);

            _storage.Setup(s => s.Resources).Returns(new List<IResource>()
            {
                _resource1.Object, _resource2.Object, _resource3.Object
            });

            _stateSerializer.Setup(ss => ss.Serialize(It.IsAny<StateRoot>())).Returns(string.Empty).Callback<StateRoot>((res) => _generatedState = res);

            _stateEncoder = new StateEncoder(_stateSerializer.Object);
        }

        [Test]
        public void StateEncoderShouldProduceProperStateRoot()
        {
            // act
            _stateEncoder.EncodeState(_storage.Object);

            // assert
            Assert.AreEqual(2, _generatedState.ResourceCollection.Resources.Count);

            Assert.AreEqual(1.0f, _generatedState.ResourceCollection.Resources[0].Amount);
            Assert.AreEqual(20.0f, _generatedState.ResourceCollection.Resources[1].Amount);

            Assert.AreEqual("Meat", _generatedState.ResourceCollection.Resources[0].Type);
            Assert.AreEqual("Stone", _generatedState.ResourceCollection.Resources[1].Type);
        }

        [Test]
        public void TestIfSerializationIsNullEncodingThrowsArgumentNullException()
        {
            // arrange
            _stateSerializer.Setup(ss => ss.Serialize(It.IsAny<StateRoot>())).Returns<string>(null);

            // act & assert
            Assert.Throws<ArgumentNullException>(() => _stateEncoder.EncodeState(_storage.Object));
        }

        [TestCase("", "")]
        [TestCase("YWI=", "ab")]
        [TestCase("MjM0", "234")]
        public void TestIfSerializationWorksFine(string expectedResult, string serializedValue)
        {
            // arrange
            _stateSerializer.Setup(ss => ss.Serialize(It.IsAny<StateRoot>())).Returns(serializedValue);

            // act
            string result = _stateEncoder.EncodeState(_storage.Object);

            // assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
