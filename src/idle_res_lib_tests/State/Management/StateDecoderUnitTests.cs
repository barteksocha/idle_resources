﻿using essentials.Wrappers;
using idle_res_lib.State.Management;
using idle_res_lib.State.Serializables;
using Moq;
using NUnit.Framework;

namespace idle_res_lib_tests.State.Management
{
    [TestFixture]
    public class StateDecoderUnitTests
    {
        private StateDecoder _stateDecoder;
        private StateRoot _stateRootResult;

        private Mock<IXmlSerializerWrapper<StateRoot>> _stateSerializer;

        [SetUp]
        public void SetUp()
        {
            _stateRootResult = new StateRoot();
            _stateSerializer = new Mock<IXmlSerializerWrapper<StateRoot>>();
            _stateSerializer.Setup(ss => ss.Deserialize(It.IsAny<string>())).Returns(_stateRootResult);

            _stateDecoder = new StateDecoder(_stateSerializer.Object);
        }

        [Test]
        public void TestIfDecodingEmptyStringReturnsEmptyResourcesState()
        {
            // act
            StateRoot result = _stateDecoder.DecodeState(string.Empty);

            // assert
            Assert.AreEqual(0, result.ResourceCollection.Resources.Count);
        }

        [Test]
        public void TestIfDecodingWorksCorrectly()
        {
            // act
            StateRoot result = _stateDecoder.DecodeState("MTE=");

            // assert
            Assert.AreEqual(_stateRootResult, result);
        }
    }
}
