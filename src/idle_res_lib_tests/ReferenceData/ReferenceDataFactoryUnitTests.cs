﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using idle_res_lib.ReferenceData;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib_tests.TestCaseSources;
using Moq;
using NUnit.Framework;

namespace idle_res_lib_tests.ReferenceData
{
    [TestFixture]
    public class ReferenceDataFactoryUnitTests
    {
        private Mock<IXmlSerializerWrapper<ReferenceDataRoot>> _xmlSerializerWrapper;
        private Mock<ILogger> _logger;

        private ReferenceDataFactory _referenceDataFactory;

        [SetUp]
        public void SetUp()
        {
            _xmlSerializerWrapper = new Mock<IXmlSerializerWrapper<ReferenceDataRoot>>();
            _logger = new Mock<ILogger>();

            _referenceDataFactory = new ReferenceDataFactory(_xmlSerializerWrapper.Object, _logger.Object);
        }

        [TestCase("Reading reference data: abc", "abc")]
        [TestCase("Reading reference data: 123513fdg", "123513fdg")]
        public void VerifyIfReferenceDataCreationIsProperlyLogged(string expectedMessage, string xmlContents)
        {
            // act
            _referenceDataFactory.GetData(xmlContents);

            // assert
            _logger.Verify(l => l.Trace(expectedMessage, null, -1), Times.Once);
        }

        [TestCaseSource(typeof(StringTestCaseSources), nameof(StringTestCaseSources.UpperLowerDigitsSpaceUpTo1000CharactersSourceOf100Strings))]
        public void VerifyIfDeserializerWasCalledWithXmlContents(string xmlContents)
        {
            // act
            _referenceDataFactory.GetData(xmlContents);

            // assert
            _xmlSerializerWrapper.Verify(xs => xs.Deserialize(xmlContents), Times.Once);
            _xmlSerializerWrapper.Verify(xs => xs.Deserialize(It.IsAny<string>()), Times.Once);
        }
    }
}
