﻿using idle_res_lib;
using System;
using System.IO;

namespace idle_res_lib_console_sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            TheHubFacade.Bootstrapper.Logger.CurrentSeverity = idle_res_lib.Logger.Severity.Trace;
            TheHubFacade.Initialize(File.ReadAllText(@".\Resources\data.xml"), "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTE2Ij8+PFN0YXRlRGF0YSB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4bWxuczp4c2Q9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIj48UmVzb3VyY2VzPjxSZXNvdXJjZT48VHlwZT5Xb29kPC9UeXBlPjxBbW91bnQ+NTwvQW1vdW50PjwvUmVzb3VyY2U+PC9SZXNvdXJjZXM+PC9TdGF0ZURhdGE+");
            TheHubFacade.Resources.ForEach(r => Console.WriteLine($"{r.Identifier.Name}:{r.Identifier.BasePrice},{r.Amount}"));
            Console.WriteLine(TheHubFacade.State);
            TheHubFacade.Resources[0].Amount += 5.0f;
            TheHubFacade.Resources.ForEach(r => Console.WriteLine($"{r.Identifier.Name}:{r.Identifier.BasePrice},{r.Amount}"));
            Console.WriteLine(TheHubFacade.State);
        }
    }
}
