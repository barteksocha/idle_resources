﻿using data_access.Access;
using idle_web.Controllers.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace idle_web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        [HttpPost("[action]")]
        public IEnumerable<bool> TryToLogin([FromBody]UserData userData)
        {
            yield return UsersDataAccess.TryToLogin(userData.Username, userData.Password);
        }

        [HttpPost("[action]")]
        public IEnumerable<bool> TryToRegister([FromBody]UserData userData)
        {
            yield return UsersDataAccess.TryToRegister(userData.Username, userData.Password);
        }
    }
}
