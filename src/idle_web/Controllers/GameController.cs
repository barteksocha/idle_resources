﻿using data_access.Access;
using idle_web.Controllers.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace idle_web.Controllers
{
    [Route("api/[controller]")]
    public class GameController
    {
        [HttpPost("[action]")]
        public IEnumerable<bool> TryToCreateGame([FromBody]GameCreationData gameCreationData)
        {
            yield return GameDataAccess.TryToCreateGame(gameCreationData.Username, gameCreationData.GameName);
        }
    }
}
