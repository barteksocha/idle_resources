﻿namespace idle_web.Controllers.Data
{
    public class GameCreationData
    {
        public string Username { get; set; }
        public string GameName { get; set; }
    }
}
