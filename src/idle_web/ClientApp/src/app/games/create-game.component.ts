import { Inject, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserData } from '../user/userdata';

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
})
export class CreateGameComponent {
  private httpClient: HttpClient;
  private baseUrl: string;

  private gameName: string;

  private userData: UserData;

  private gameCreatedSuccessfully: boolean;

  constructor(httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.httpClient = httpClient;
    this.baseUrl = baseUrl;

    this.gameCreatedSuccessfully = true;

    this.userData = new UserData();

    this.redirectIfNotLoggedIn();
  }

  private redirectIfNotLoggedIn() {
    if (!this.userData.isUserLoggedIn()) {
      window.location.href = "/";
    }
  }

  private redirectToChooseGamePage() {
    window.location.href = "/select_game";
  }

  private tryToCreateGame() {
    var data = { username: this.userData.getLoggedUser(), gameName: this.gameName };
    this.httpClient.post<boolean>(this.baseUrl + 'api/Game/TryToCreateGame', data).subscribe(result => {
      this.gameCreatedSuccessfully = result[0];

      if (this.gameCreatedSuccessfully) {
        this.redirectToChooseGamePage();
      }
    }, error => console.error(error));
  }
}
