import { Component } from '@angular/core';
import { UserData } from './../user/userdata';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  public isExpanded = false;
  public isLoggedIn = false;

  public profileNameDescription: string;

  private userData = new UserData();

  constructor() {
    this.isLoggedIn = this.userData.isUserLoggedIn();
    if (this.isLoggedIn) {
      this.profileNameDescription = this.userData.getLoggedUser();
    } else {
      this.profileNameDescription = "Home";
    }
  }

  private logOutAndRefresh() {
    this.userData.setLoggedUser("");
    this.refresh();
  }

  private refresh() {
    window.location.reload();
  }

  private collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
