import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserData } from './../userdata';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  private httpClient: HttpClient;
  private baseUrl: string;

  public username: string;
  public password: string;

  public userData: UserData;

  constructor(httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.httpClient = httpClient;
    this.baseUrl = baseUrl;
    this.userData = new UserData();

    this.redirectIfLoggedIn();
  }

  private redirectIfLoggedIn() {
    if (this.userData.isUserLoggedIn()) {
      window.location.href = "/";
    }
  }

  private tryToLogin() {
    var data = { username: this.username, password: this.password };
    this.httpClient.post<boolean>(this.baseUrl + 'api/User/TryToLogin', data).subscribe(result => {
      if (result[0]) {
        this.userData.setLoggedUser(this.username);
        this.redirectIfLoggedIn();
      }
    }, error => console.error(error));
  }
}
