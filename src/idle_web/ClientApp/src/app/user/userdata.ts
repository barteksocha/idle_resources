export class UserData {
  constructor() {
  }

  public isUserLoggedIn() {
    var loggedUser = this.getLoggedUser();
    var isLoggedIn = false;
    if (loggedUser) {
      isLoggedIn = true;
    }

    return isLoggedIn;
  }

  public getLoggedUser() {
    return localStorage.getItem("Username");
  }

  public setLoggedUser(newLoggedUser: string) {
    localStorage.setItem("Username", newLoggedUser);
  }
}
