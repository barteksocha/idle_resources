import { Inject, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
})

export class RegistrationComponent {
  private httpClient: HttpClient;
  private baseUrl: string;

  private username: string;
  private password: string;

  private hasRegistrationFailed: boolean;
  private hasRegistrationSucceeded: boolean;

  constructor(httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.httpClient = httpClient;
    this.baseUrl = baseUrl;

    this.hasRegistrationFailed = false;
    this.hasRegistrationSucceeded = false;
  }

  private tryToRegister() {
    var data = { username: this.username, password: this.password };
    this.httpClient.post<boolean>(this.baseUrl + 'api/User/TryToRegister', data).subscribe(result => {
      if (result[0]) {
        this.hasRegistrationSucceeded = true;
        this.hasRegistrationFailed = false;
      }
      else {
        this.hasRegistrationSucceeded = false;
        this.hasRegistrationFailed = true;
      }
    }, error => console.error(error));
  }
}
