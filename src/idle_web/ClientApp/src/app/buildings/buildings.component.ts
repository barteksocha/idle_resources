import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserData } from './../user/userdata';

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html'
})
export class BuildingsComponent {
  public forecasts: WeatherForecast[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<WeatherForecast[]>(baseUrl + 'api/SampleData/WeatherForecasts').subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));

    this.redirectIfNotLoggedIn();
  }

  private redirectIfNotLoggedIn() {
    var userData = new UserData();

    if (!userData.isUserLoggedIn()) {
      window.location.href = "/login";
    }
  }
}

interface WeatherForecast {
  dateFormatted: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
