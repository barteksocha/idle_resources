﻿using data_access.Encryption;
using idle_web.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace data_access.Access
{
    public static class UsersDataAccess
    {
        public static bool TryToLogin(string username, string password)
        {
            string hashedPassword = DataEncryptor.Encrypt(username, password, MultipleKeysEncryptionType.Key2ConcatForHashedValue);
            using (idle_dbContext context = new idle_dbContext())
            {
                return context.Users.Any(u => u.Name == username && u.Password == hashedPassword);
            }
        }

        public static bool TryToRegister(string username, string password)
        {
            try
            {
                string hashedPassword = DataEncryptor.Encrypt(username, password, MultipleKeysEncryptionType.Key2ConcatForHashedValue);
                using (idle_dbContext context = new idle_dbContext())
                {
                    context.Users.Add(new Users { Name = username, Password = hashedPassword });
                    return context.SaveChanges() > 0;
                }
            }
            catch (DbUpdateException)
            {
                return false;
            }
        }
    }
}
