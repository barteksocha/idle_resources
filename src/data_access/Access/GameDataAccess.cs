﻿using idle_web.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace data_access.Access
{
    public static class GameDataAccess
    {
        public static bool TryToCreateGame(string username, string gameName)
        {
            try
            {
                using (idle_dbContext context = new idle_dbContext())
                {
                    context.Users.First(u => u.Name == username).Games.Add(new Games
                    {
                        Name = gameName
                    });
                    return context.SaveChanges() > 0;
                }
            }
            catch (DbUpdateException)
            {
                return false;
            }
        }
    }
}
