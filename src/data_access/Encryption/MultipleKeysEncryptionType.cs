﻿using System;

namespace data_access.Encryption
{
    [Flags]
    public enum MultipleKeysEncryptionType
    {
        None = 0,
        Key1ConcatForHashedValue = 1 << 1,
        Key2ConcatForHashedValue = 1 << 2
    }
}
