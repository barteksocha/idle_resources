﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace data_access.Encryption
{
    internal class DataEncryptor
    {
        public static string Encrypt(string key1, string key2, MultipleKeysEncryptionType encrpytionType)
        {
            return Encrypt(Encoding.ASCII.GetBytes(key1), Encoding.ASCII.GetBytes(key2), encrpytionType);
        }

        public static string Encrypt(byte[] key1, byte[] key2, MultipleKeysEncryptionType encrpytionType)
        {
            byte[] mixedArray = HashMixArrays(key1, key2);

            if (encrpytionType.HasFlag(MultipleKeysEncryptionType.Key1ConcatForHashedValue))
            {
                mixedArray = mixedArray.Concat(key1).ToArray();
            }

            if (encrpytionType.HasFlag(MultipleKeysEncryptionType.Key2ConcatForHashedValue))
            {
                mixedArray = mixedArray.Concat(key2).ToArray();
            }

            using (SHA512 shaM = new SHA512Managed())
            {
                return Convert.ToBase64String(shaM.ComputeHash(mixedArray));
            }
        }

        private static byte[] HashMixArrays(byte[] array1, byte[] array2)
        {
            const int mixedArrayLength = 20;
            byte[] result = new byte[mixedArrayLength];

            byte[] hashArraySource = array1.Concat(array2).ToArray();
            int hashValue = 0;

            foreach (byte hashSource in hashArraySource)
            {
                hashValue = (((hashSource & hashValue) + (hashValue & (~0xFF))) << 2) + (hashSource | (hashValue & 0xFF));
            }

            Random random = new Random(hashValue);

            for (int i = 0; i < mixedArrayLength; ++i)
            {
                if (i % 2 == 0)
                {
                    result[i] = array1[random.Next() % array1.Length];
                }
                else
                {
                    result[i] = array2[random.Next() % array2.Length];
                }
            }

            return result;
        }
    }
}
