CREATE TABLE Users (
	UserId int IDENTITY(1,1),
	Name varchar(255) NOT NULL,
	Password varchar(1000) NOT NULL,
	UNIQUE(Name),
	PRIMARY KEY(UserId)
);

CREATE TABLE Games (
	GameId int IDENTITY(1,1),
	Name varchar(255) NOT NULL,
	GameState varchar(2000),
	UserId int NOT NULL,
	PRIMARY KEY(GameId),
	FOREIGN KEY(UserId) REFERENCES Users(UserId)
);