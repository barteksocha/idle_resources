﻿using System;
using System.Collections.Generic;

namespace idle_web.Models
{
    public partial class Users
    {
        public Users()
        {
            Games = new HashSet<Games>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public ICollection<Games> Games { get; set; }
    }
}
