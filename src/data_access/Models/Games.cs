﻿using System;
using System.Collections.Generic;

namespace idle_web.Models
{
    public partial class Games
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public string GameState { get; set; }
        public int UserId { get; set; }

        public Users User { get; set; }
    }
}
