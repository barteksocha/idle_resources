﻿using idle_res_lib.Resources;
using System;
using System.Collections.Generic;

namespace idle_res_lib
{
    public static class TheHubFacade
    {
        private static readonly Lazy<TheHub> hub = new Lazy<TheHub>(() => new TheHub(Bootstrapper));

        /// Making any changes in bootstrapper or maybe even changing some properties of bootstrapper will influence the hub only before any method
        /// working on the hub (this is almost everyone in the facade!) hasn't been called yet!!!
        /// If you want to apply some changes in the bootstrapper, make it FIRST, before any work with the facade, this is quite logic, but worth of accent
        public static IBootstrapper Bootstrapper { get; set; } = new Bootstrapper();

        public static List<IResource> Resources => hub.Value.Resources;

        public static string State => hub.Value.State;

        public static void Initialize(string referenceDataXmlContents, string encodedState)
        {
            hub.Value.Initialize(referenceDataXmlContents, encodedState);
        }
    }
}
