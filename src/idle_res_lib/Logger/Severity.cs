﻿namespace idle_res_lib.Logger
{
    public enum Severity
    {
        Unknown,
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal
    }
}
