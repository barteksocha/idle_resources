﻿using essentials.Wrappers;
using System.Runtime.CompilerServices;

namespace idle_res_lib.Logger
{
    public class VisualStudioConsoleLogger : ILogger
    {
        private readonly IDateTimeWrapper _dateTime;
        private readonly IDebugDiagnosticsWrapper _debugDiagnostics;

        public VisualStudioConsoleLogger(IDateTimeWrapper dateTime, IDebugDiagnosticsWrapper debugDiagnostics)
        {
            _dateTime = dateTime;
            _debugDiagnostics = debugDiagnostics;
            CurrentSeverity = Severity.Warning;
        }

        public Severity CurrentSeverity { get; set; }

        public void Debug(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Debug, memberName, line);
        }

        public void Error(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Error, memberName, line);
        }

        public void Fatal(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Fatal, memberName, line);
        }

        public void Info(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Info, memberName, line);
        }

        public void Trace(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Trace, memberName, line);
        }

        public void Warning(string message, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            Log(message, Severity.Warning, memberName, line);
        }

        public void Log(string message, Severity severity, [CallerMemberName]string memberName = null, [CallerLineNumber]int line = -1)
        {
            if (severity >= CurrentSeverity)
            {
                string fullMessage;
                if (!string.IsNullOrEmpty(memberName) && line >= 0)
                {
                    fullMessage = $"[{_dateTime.Now.ToString()}@{memberName}@{line}]{severity.ToString().ToUpper()}: {message}";
                }
                else
                {
                    fullMessage = $"[{_dateTime.Now}]{severity.ToString().ToUpper()}: {message}";
                }

                _debugDiagnostics.WriteLine(fullMessage);
            }
        }
    }
}
