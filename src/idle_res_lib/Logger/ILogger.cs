﻿namespace idle_res_lib.Logger
{
    public interface ILogger
    {
        Severity CurrentSeverity { get; set; }

        void Trace(string message, string memberName = null, int line = -1);
        void Debug(string message, string memberName = null, int line = -1);
        void Info(string message, string memberName = null, int line = -1);
        void Warning(string message, string memberName = null, int line = -1);
        void Error(string message, string memberName = null, int line = -1);
        void Fatal(string message, string memberName = null, int line = -1);
    }
}
