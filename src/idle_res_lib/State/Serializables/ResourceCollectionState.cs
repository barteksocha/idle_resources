﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace idle_res_lib.State.Serializables
{
    public class ResourceCollectionState
    {
        [XmlElement("Resource")]
        public List<ResourceState> Resources { get; set; }
    }
}
