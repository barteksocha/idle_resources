﻿using System.Xml.Serialization;

namespace idle_res_lib.State.Serializables
{
    [XmlRoot("StateData", Namespace = "", IsNullable = false)]
    public class StateRoot
    {
        [XmlElement("Resources")]
        public ResourceCollectionState ResourceCollection { get; set; }
    }
}
