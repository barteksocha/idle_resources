﻿using System.Xml.Serialization;

namespace idle_res_lib.State.Serializables
{
    public class ResourceState
    {
        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlElement("Amount")]
        public float Amount { get; set; }
    }
}
