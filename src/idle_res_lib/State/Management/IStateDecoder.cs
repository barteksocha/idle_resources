﻿using idle_res_lib.State.Serializables;

namespace idle_res_lib.State.Management
{
    public interface IStateDecoder
    {
        StateRoot DecodeState(string encodedState);
    }
}
