﻿using essentials.Wrappers;
using idle_res_lib.State.Serializables;
using System;
using System.Collections.Generic;
using System.Text;

namespace idle_res_lib.State.Management
{
    public class StateDecoder : IStateDecoder
    {
        private readonly IXmlSerializerWrapper<StateRoot> _xmlSerializerWrapper;

        public StateDecoder(IXmlSerializerWrapper<StateRoot> xmlSerializerWrapper)
        {
            _xmlSerializerWrapper = xmlSerializerWrapper;
        }

        public StateRoot DecodeState(string encodedState)
        {
            byte[] data = Convert.FromBase64String(encodedState);
            string decodedData = Encoding.UTF8.GetString(data);

            if (decodedData == string.Empty)
            {
                return GetStartingStateRoot();
            }
            else
            {
                return _xmlSerializerWrapper.Deserialize(decodedData);
            }
        }

        private StateRoot GetStartingStateRoot()
        {
            return new StateRoot
            {
                ResourceCollection = new ResourceCollectionState
                {
                    Resources = new List<ResourceState>()
                }
            };
        }
    }
}
