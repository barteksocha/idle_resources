﻿using idle_res_lib.Resources;

namespace idle_res_lib.State.Management
{
    public interface IStateEncoder
    {
        string EncodeState(IStorage storage);
    }
}
