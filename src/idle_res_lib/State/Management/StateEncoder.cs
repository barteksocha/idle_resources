﻿using essentials.Wrappers;
using idle_res_lib.Resources;
using idle_res_lib.State.Serializables;
using System;
using System.Linq;
using System.Text;

namespace idle_res_lib.State.Management
{
    public class StateEncoder : IStateEncoder
    {
        private readonly IXmlSerializerWrapper<StateRoot> _xmlSerializerWrapper;

        public StateEncoder(IXmlSerializerWrapper<StateRoot> xmlSerializerWrapper)
        {
            _xmlSerializerWrapper = xmlSerializerWrapper;
        }

        public string EncodeState(IStorage storage)
        {
            StateRoot stateRoot = GetStateRoot(storage);
            string serializedData = _xmlSerializerWrapper.Serialize(stateRoot);
            byte[] rawSerializedData = Encoding.UTF8.GetBytes(serializedData);
            return Convert.ToBase64String(rawSerializedData);
        }

        private StateRoot GetStateRoot(IStorage storage)
        {
            return new StateRoot
            {
                ResourceCollection = new ResourceCollectionState
                {
                    Resources = storage.Resources.Where(r => r.Amount > 0.0f).Select(r => GetResourceState(r)).ToList()
                }
            };
        }

        private ResourceState GetResourceState(IResource resource)
        {
            return new ResourceState
            {
                Amount = resource.Amount,
                Type = resource.Identifier.Type.ToString()
            };
        }
    }
}
