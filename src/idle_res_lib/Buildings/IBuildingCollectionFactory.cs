﻿using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.State.Serializables;
using System.Collections.Generic;

namespace idle_res_lib.Buildings
{
    public interface IBuildingCollectionFactory
    {
        Dictionary<BuildingType, IBuilding> CreateBuilding(ReferenceDataRoot referenceData, StateRoot stateData);
    }
}
