﻿using idle_res_lib.Resources;
using System.Collections.Generic;

namespace idle_res_lib.Buildings
{
    public interface IBuilding
    {
        BuildingType BuildingType { get; }
        string Name { get; }
        int Quantity { get; }
        List<IResourceProducer> ResourceProducers { get; }
    }
}
