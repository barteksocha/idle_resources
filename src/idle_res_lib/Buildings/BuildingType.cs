﻿namespace idle_res_lib.Buildings
{
    public enum BuildingType
    {
        Woodcutter,
        GathererHut,
        WoodenToolMaker
    }
}
