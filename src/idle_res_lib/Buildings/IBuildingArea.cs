﻿using System.Collections.Generic;

namespace idle_res_lib.Buildings
{
    public interface IBuildingArea
    {
        Dictionary<BuildingType, IBuilding> Buildings { get; }
    }
}
