﻿namespace idle_res_lib.Prestige
{
    public interface IPrestigeDataProvider
    {
        float PrestigePoints { get; }
        float PrestigeMultiplier { get; }
    }
}
