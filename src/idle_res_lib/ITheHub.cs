﻿using idle_res_lib.Resources;
using System.Collections.Generic;

namespace idle_res_lib
{
    public interface ITheHub
    {
        List<IResource> Resources { get; }
        string State { get; }

        void Initialize(string referenceDataXmlContents, string encodedState);
    }
}
