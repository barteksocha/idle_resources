﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using idle_res_lib.ReferenceData.Serializables;

namespace idle_res_lib.ReferenceData
{
    public class ReferenceDataFactory : IReferenceDataFactory
    {
        private readonly IXmlSerializerWrapper<ReferenceDataRoot> _xmlSerializerWrapper;
        private readonly ILogger _logger;

        public ReferenceDataFactory(IXmlSerializerWrapper<ReferenceDataRoot> xmlSerializerWrapper, ILogger logger)
        {
            _xmlSerializerWrapper = xmlSerializerWrapper;
            _logger = logger;
        }

        public ReferenceDataRoot GetData(string xmlFileContents)
        {
            _logger.Trace($"Reading reference data: {xmlFileContents}");
            return _xmlSerializerWrapper.Deserialize(xmlFileContents);
        }
    }
}
