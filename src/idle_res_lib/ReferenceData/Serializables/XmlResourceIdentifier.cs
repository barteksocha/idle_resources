﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlResourceIdentifier
    {
        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlElement("Price")]
        public float Price { get; set; }

        [XmlElement("Tier")]
        public int TierIndex { get; set; }
    }
}
