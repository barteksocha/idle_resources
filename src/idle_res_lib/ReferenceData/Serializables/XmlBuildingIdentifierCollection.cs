﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlBuildingIdentifierCollection
    {
        [XmlElement("Building")]
        public XmlBuildingIdentifier[] BuildingIdentifiers { get; set; }
    }
}
