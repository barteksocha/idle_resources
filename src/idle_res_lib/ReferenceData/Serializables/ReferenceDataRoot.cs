﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    [XmlRoot("GameData", Namespace = "", IsNullable = false)]
    public class ReferenceDataRoot
    {
        [XmlElement("Resources")]
        public XmlResourceIdentifierCollection Resources { get; set; }

        [XmlElement("Buildings")]
        public XmlBuildingIdentifierCollection Buildings { get; set; }
    }
}
