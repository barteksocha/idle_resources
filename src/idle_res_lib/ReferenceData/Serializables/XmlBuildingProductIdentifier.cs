﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlBuildingProductIdentifier
    {
        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlElement("BaseAmount")]
        public float BaseAmount { get; set; }
    }
}
