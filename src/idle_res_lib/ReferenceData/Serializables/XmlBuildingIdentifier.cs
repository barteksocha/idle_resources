﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlBuildingIdentifier
    {
        [XmlElement("Type")]
        public string Type { get; set; }

        [XmlElement("Products")]
        public XmlBuildingProductIdentifierCollection Products { get; set; }

        [XmlElement("Consumed")]
        public XmlBuildingProductIdentifierCollection ConsumedProducts { get; set; }
    }
}
