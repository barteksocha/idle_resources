﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlResourceIdentifierCollection
    {
        [XmlElement("Resource", typeof(XmlResourceIdentifier))]
        public XmlResourceIdentifier[] ResourceIdentifiers { get; set; }
    }
}
