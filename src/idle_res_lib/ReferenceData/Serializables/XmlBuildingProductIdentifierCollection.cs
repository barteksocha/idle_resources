﻿using System.Xml.Serialization;

namespace idle_res_lib.ReferenceData.Serializables
{
    public class XmlBuildingProductIdentifierCollection
    {
        [XmlElement("Product")]
        public XmlBuildingProductIdentifier[] Products { get; set; }
    }
}
