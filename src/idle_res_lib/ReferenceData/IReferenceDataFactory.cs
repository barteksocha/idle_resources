﻿using idle_res_lib.ReferenceData.Serializables;

namespace idle_res_lib.ReferenceData
{
    public interface IReferenceDataFactory
    {
        ReferenceDataRoot GetData(string xmlFileContents);
    }
}
