﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using idle_res_lib.ReferenceData;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.Resources;
using idle_res_lib.State.Management;
using idle_res_lib.State.Serializables;
using System.Reflection;

namespace idle_res_lib
{
    public class Bootstrapper : IBootstrapper
    {
        public Bootstrapper()
        {
            InitializeBootstrapper();
        }

        public ILogger Logger { get; set; }

        public IStorage Storage { get; private set; }

        public IReferenceDataFactory ReferenceDataFactory { get; private set; }

        public IStateDecoder StateDecoder { get; private set; }

        public IStateEncoder StateEncoder { get; private set; }

        private void InitializeBootstrapper()
        {
            IDateTimeWrapper dateTimeWrapper = new DateTimeWrapper();
            IDebugDiagnosticsWrapper debugDiagnostics = new DebugDiagnosticsWrapper();

            if (Logger == null)
            {
                Logger = new VisualStudioConsoleLogger(dateTimeWrapper, debugDiagnostics);
            }

            IAssemblyWrapper thisAssembly = new AssemblyWrapper(Assembly.GetExecutingAssembly());
            IActivatorWrapper activator = new ActivatorWrapper();

            IXmlSerializerWrapper<ReferenceDataRoot> referenceDataXmlSerializer = new XmlSerializerWrapper<ReferenceDataRoot>();
            IXmlSerializerWrapper<StateRoot> stateDataXmlSerializer = new XmlSerializerWrapper<StateRoot>();

            IResourceIdentifierFactory resourceIdentifierFactory = new ResourceIdentifierFactory();
            IResourceFactory resourceFactory = new ResourceFactory(Logger);

            IResourceCollectionFactory resourceCollectionFactory = new ResourceCollectionFactory(Logger, resourceIdentifierFactory, resourceFactory);

            Storage = new Storage(Logger, resourceCollectionFactory);
            ReferenceDataFactory = new ReferenceDataFactory(referenceDataXmlSerializer, Logger);
            StateDecoder = new StateDecoder(stateDataXmlSerializer);
            StateEncoder = new StateEncoder(stateDataXmlSerializer);
        }
    }
}
