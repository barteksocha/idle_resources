﻿using idle_res_lib.Logger;
using idle_res_lib.ReferenceData;
using idle_res_lib.Resources;
using idle_res_lib.State.Management;

namespace idle_res_lib
{
    public interface IBootstrapper
    {
        ILogger Logger { get; set; }
        IStorage Storage { get; }
        IReferenceDataFactory ReferenceDataFactory { get; }
        IStateDecoder StateDecoder { get; }
        IStateEncoder StateEncoder { get; }
    }
}
