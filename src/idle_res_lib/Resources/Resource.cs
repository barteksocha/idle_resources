﻿namespace idle_res_lib.Resources
{
    public class Resource : IResource
    {
        public Resource(IResourceIdentifier identifier, float amount)
        {
            Identifier = identifier;
            Amount = amount;
        }

        public IResourceIdentifier Identifier { get; set; }

        public float Amount { get; set; }

        public float Growth => 0.0f;
    }
}
