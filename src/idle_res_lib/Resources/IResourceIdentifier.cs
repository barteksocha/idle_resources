﻿using idle_res_lib.Tiers;

namespace idle_res_lib.Resources
{
    public interface IResourceIdentifier
    {
        ResourceType Type { get; }
        string Name { get; }
        float BasePrice { get; }
        TierType Tier { get; }
    }
}
