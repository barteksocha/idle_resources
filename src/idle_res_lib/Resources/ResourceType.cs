﻿namespace idle_res_lib.Resources
{
    public enum ResourceType
    {
        Unknown,
        Wood,
        ForestFruits,
        ForestVegetables,
        WoodenTools,
        WoodenWeapons,
        Meat,
        Stone,
        Potion,
        Herbs,
        Science
    }
}
