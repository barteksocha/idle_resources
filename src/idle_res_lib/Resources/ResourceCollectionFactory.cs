﻿using idle_res_lib.Logger;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.State.Serializables;
using System.Collections.Generic;
using System.Linq;

namespace idle_res_lib.Resources
{
    public class ResourceCollectionFactory : IResourceCollectionFactory
    {
        private readonly ILogger _logger;
        private readonly IResourceIdentifierFactory _resourceIdentifierFactory;
        private readonly IResourceFactory _resourceFactory;

        public ResourceCollectionFactory(ILogger logger, IResourceIdentifierFactory resourceIdentifierFactory, IResourceFactory resourceFactory)
        {
            _logger = logger;
            _resourceIdentifierFactory = resourceIdentifierFactory;
            _resourceFactory = resourceFactory;
        }

        public List<IResource> CreateResources(ReferenceDataRoot referenceData, StateRoot stateData)
        {
            _logger.Info("Creating resource collection");
            return _resourceIdentifierFactory.Create(referenceData).Select(ri => _resourceFactory.CreateResource(ri, stateData)).ToList();
        }
    }
}
