﻿namespace idle_res_lib.Resources
{
    public interface IResourceProducer
    {
        void ProduceResources();
    }
}
