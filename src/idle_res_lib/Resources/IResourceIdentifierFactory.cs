﻿using idle_res_lib.ReferenceData.Serializables;
using System.Collections.Generic;

namespace idle_res_lib.Resources
{
    public interface IResourceIdentifierFactory
    {
        List<IResourceIdentifier> Create(ReferenceDataRoot referenceData);
    }
}
