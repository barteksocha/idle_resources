﻿using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.State.Serializables;
using System.Collections.Generic;

namespace idle_res_lib.Resources
{
    public interface IResourceCollectionFactory
    {
        List<IResource> CreateResources(ReferenceDataRoot referenceData, StateRoot stateData);
    }
}
