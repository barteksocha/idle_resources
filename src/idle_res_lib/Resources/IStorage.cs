﻿using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.State.Serializables;
using System.Collections.Generic;

namespace idle_res_lib.Resources
{
    public interface IStorage
    {
        List<IResource> Resources { get; set; }

        void InitializeResources(ReferenceDataRoot referenceData, StateRoot stateData);
    }
}
