﻿namespace idle_res_lib.Resources
{
    public interface IResource
    {
        IResourceIdentifier Identifier { get; set; }
        float Amount { get; set; }
        float Growth { get; } 
    }
}
