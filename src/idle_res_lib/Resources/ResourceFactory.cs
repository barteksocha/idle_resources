﻿using essentials.Wrappers;
using idle_res_lib.Logger;
using idle_res_lib.State.Serializables;
using System.Collections.Generic;
using System.Linq;

namespace idle_res_lib.Resources
{
    public class ResourceFactory : IResourceFactory
    {
        private readonly ILogger _logger;

        public ResourceFactory(ILogger logger)
        {
            _logger = logger;
        }

        public IResource CreateResource(IResourceIdentifier identifier, StateRoot stateData)
        {
            _logger.Info($"Creating resource {identifier.Type.ToString()}");

            ResourceState resourceState = stateData.ResourceCollection.Resources.FirstOrDefault(r => r.Type == identifier.Type.ToString());
            float amount = 0.0f;

            if (resourceState != null)
            {
                _logger.Debug($"Resource {identifier.Type.ToString()} starting amount {resourceState.Amount}");
                amount = resourceState.Amount;
            }

            return new Resource(identifier, amount);
        }
    }
}
