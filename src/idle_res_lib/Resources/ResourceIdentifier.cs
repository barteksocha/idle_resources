﻿using idle_res_lib.Tiers;

namespace idle_res_lib.Resources
{
    public class ResourceIdentifier : IResourceIdentifier
    {
        public ResourceIdentifier(ResourceType type, string name, float basePrice, TierType tier)
        {
            Type = type;
            Name = name;
            BasePrice = basePrice;
            Tier = tier;
        }

        public ResourceType Type { get; private set; }

        public string Name { get; private set; }

        public float BasePrice { get; private set; }

        public TierType Tier { get; private set; }
    }
}
