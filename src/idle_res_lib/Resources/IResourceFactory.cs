﻿using idle_res_lib.State.Serializables;

namespace idle_res_lib.Resources
{
    public interface IResourceFactory
    {
        IResource CreateResource(IResourceIdentifier identifier, StateRoot stateData);
    }
}
