﻿namespace idle_res_lib.Resources
{
    public interface IResourceConsumer
    {
        float ConsumeResources();
    }
}
