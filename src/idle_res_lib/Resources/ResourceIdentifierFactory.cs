﻿using essentials.Commons;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.Tiers;
using System.Collections.Generic;
using System.Linq;

namespace idle_res_lib.Resources
{
    public class ResourceIdentifierFactory : IResourceIdentifierFactory
    {
        public List<IResourceIdentifier> Create(ReferenceDataRoot referenceData)
        {
            return referenceData.Resources.ResourceIdentifiers.Select(ri => new ResourceIdentifier(ri.Type.ToEnum<ResourceType>(), ri.Type, ri.Price, (TierType)ri.TierIndex)).Cast<IResourceIdentifier>().ToList();
        }
    }
}
