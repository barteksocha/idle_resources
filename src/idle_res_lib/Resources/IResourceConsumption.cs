﻿namespace idle_res_lib.Resources
{
    public interface IResourceConsumption
    {
        IResourceIdentifier Resource { get; }
        float BaseFactor { get; }
    }
}
