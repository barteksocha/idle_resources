﻿using System.Collections.Generic;
using idle_res_lib.Logger;
using idle_res_lib.ReferenceData.Serializables;
using idle_res_lib.State.Serializables;

namespace idle_res_lib.Resources
{
    public class Storage : IStorage
    {
        private readonly ILogger _logger;
        private readonly IResourceCollectionFactory _resourceCollectionFactory;

        public Storage(ILogger logger, IResourceCollectionFactory resourceCollectionFactory)
        {
            _logger = logger;
            _resourceCollectionFactory = resourceCollectionFactory;
        }

        public List<IResource> Resources { get; set; }

        public void InitializeResources(ReferenceDataRoot referenceData, StateRoot stateData)
        {
            _logger.Info($"Initializing storage");
            Resources = _resourceCollectionFactory.CreateResources(referenceData, stateData);
        }
    }
}
