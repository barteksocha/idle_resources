﻿namespace idle_res_lib.Tiers
{
    public enum TierType
    {
        Unknown = 0,
        PreStoneAge = 1,
        StoneAge = 2
    }
}
