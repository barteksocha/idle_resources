﻿using System.Collections.Generic;
using idle_res_lib.Logger;
using idle_res_lib.Resources;

namespace idle_res_lib
{
    public class TheHub : ITheHub
    {
        private readonly ILogger _logger;
        private readonly IBootstrapper _bootstrapper;
        private readonly IStorage _storage;

        public TheHub(IBootstrapper bootstrapper)
        {
            _logger = bootstrapper.Logger;
            _bootstrapper = bootstrapper;
            _storage = bootstrapper.Storage;
        }

        public List<IResource> Resources => _storage.Resources;

        public string State => _bootstrapper.StateEncoder.EncodeState(_storage);

        public void Initialize(string referenceDataXmlContents, string encodedState)
        {
            _storage.InitializeResources(_bootstrapper.ReferenceDataFactory.GetData(referenceDataXmlContents), _bootstrapper.StateDecoder.DecodeState(encodedState));
        }
    }
}
