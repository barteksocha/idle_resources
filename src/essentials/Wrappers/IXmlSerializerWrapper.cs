﻿namespace essentials.Wrappers
{
    public interface IXmlSerializerWrapper<T>
    {
        T Deserialize(string fileContents);
        string Serialize(T serializedValue);
    }
}
