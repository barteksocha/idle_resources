﻿using System;

namespace essentials.Wrappers
{
    public interface IAttributeWrapper<T> where T : Attribute
    {
        T GetCustomAttribute(Type type);
    }
}
