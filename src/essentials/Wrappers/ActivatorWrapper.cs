﻿using System;

namespace essentials.Wrappers
{
    public class ActivatorWrapper : IActivatorWrapper
    {
        public object CreateInstance(Type type)
        {
            return Activator.CreateInstance(type);
        }

        public T CreateInstance<T>(Type type)
        {
            return (T)CreateInstance(type);
        }
    }
}
