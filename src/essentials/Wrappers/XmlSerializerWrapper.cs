﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace essentials.Wrappers
{
    public class XmlSerializerWrapper<T> : IXmlSerializerWrapper<T>
    {
        private readonly XmlSerializer _xmlSerializer;

        public XmlSerializerWrapper()
        {
            _xmlSerializer = new XmlSerializer(typeof(T));
        }

        public T Deserialize(string fileContents)
        {
            T result;

            using (XmlReader reader = XmlReader.Create(new StringReader(fileContents)))
            {
                result = (T)_xmlSerializer.Deserialize(reader);
            }

            return result;
        }

        public string Serialize(T serializedValue)
        {
            StringBuilder result = new StringBuilder();

            using (XmlWriter writer = XmlWriter.Create(new StringWriter(result)))
            {
                _xmlSerializer.Serialize(writer, serializedValue);
            }

            return result.ToString();
        }
    }
}
