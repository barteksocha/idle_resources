﻿using System;
using System.Collections.Generic;

namespace essentials.Wrappers
{
    public interface IAssemblyWrapper
    {
        List<Type> GetAssemblyTypes();
    }
}
