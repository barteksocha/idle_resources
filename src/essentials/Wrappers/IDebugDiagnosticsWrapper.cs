﻿namespace essentials.Wrappers
{
    public interface IDebugDiagnosticsWrapper
    {
        void WriteLine(string message);
    }
}
