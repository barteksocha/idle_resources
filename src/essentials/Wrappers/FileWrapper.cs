﻿using System.IO;

namespace essentials.Wrappers
{
    public class FileWrapper : IFileWrapper
    {
        private readonly string _filePath;

        public FileWrapper(string filePath)
        {
            _filePath = filePath;
        }

        public string GetFileContents()
        {
            return File.ReadAllText(_filePath);
        }
    }
}
