﻿namespace essentials.Wrappers
{
    public class DebugDiagnosticsWrapper : IDebugDiagnosticsWrapper
    {
        public void WriteLine(string message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }
    }
}
