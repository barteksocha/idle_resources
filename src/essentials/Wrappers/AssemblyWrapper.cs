﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace essentials.Wrappers
{
    public class AssemblyWrapper : IAssemblyWrapper
    {
        private readonly Assembly _assembly;

        public AssemblyWrapper(Assembly assembly)
        {
            _assembly = assembly;
        }

        public List<Type> GetAssemblyTypes()
        {
            return _assembly.GetTypes().ToList();
        }
    }
}
