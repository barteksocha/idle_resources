﻿using System;

namespace essentials.Wrappers
{
    public interface IDateTimeWrapper
    {
        DateTime Now { get; }
    }
}
