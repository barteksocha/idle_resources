﻿namespace essentials.Wrappers
{
    public interface IFileWrapper
    {
        string GetFileContents();
    }
}
