﻿using System;

namespace essentials.Wrappers
{
    public class AttributeWrapper<T> : IAttributeWrapper<T> where T : Attribute
    {
        public T GetCustomAttribute(Type type)
        {
            return (T)Attribute.GetCustomAttribute(type, typeof(T));
        }
    }
}
