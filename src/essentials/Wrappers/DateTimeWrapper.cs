﻿using System;

namespace essentials.Wrappers
{
    public class DateTimeWrapper : IDateTimeWrapper
    {
        public DateTime Now => DateTime.Now;
    }
}
