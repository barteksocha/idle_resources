﻿using System;

namespace essentials.Wrappers
{
    public interface IActivatorWrapper
    {
        object CreateInstance(Type type);
        T CreateInstance<T>(Type type);
    }
}
