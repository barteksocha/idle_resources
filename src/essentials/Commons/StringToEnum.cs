﻿using System;

namespace essentials.Commons
{
    public static class StringToEnum
    {
        public static T ToEnum<T>(this string rawEnum) where T : struct
        {
            return (T)Enum.Parse(typeof(T), rawEnum, true);
        }
    }
}
